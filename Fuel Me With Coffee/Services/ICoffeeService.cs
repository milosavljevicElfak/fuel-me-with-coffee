﻿using Fuel_Me_With_Coffee.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fuel_Me_With_Coffee.Services
{
    public interface ICoffeeService
    {
        public Task Create(Coffee coffee);

        public Task<IList<CoffeeViewModel>> GetAll();

        public Task<CoffeeViewModel> GetById(string id);

        public Task Update(Coffee updatedCoffee, string idToUpdate);

        public Task Delete(string id);
    }
}
