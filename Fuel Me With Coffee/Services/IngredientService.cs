﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fuel_Me_With_Coffee.Models;
using MongoDB.Bson;

namespace Fuel_Me_With_Coffee.Services
{

    public class IngredientService : IIngredientService
    {
        private readonly IMongoClient _mongoClient;
        private readonly IMongoDatabase _mongoDatabase;

        public IngredientService(IMongoClient mongoClient)
        {
            _mongoClient = mongoClient;
            _mongoDatabase = _mongoClient.GetDatabase(MongoDbSettings.DatabaseName);
        }

        public async Task Create(Ingredient ingredient)
        {
            var ingredients = _mongoDatabase.GetCollection<Ingredient>(MongoDbSettings.IngredientCollection);
            await ingredients.InsertOneAsync(ingredient);
        }

        public async Task Delete(string id)
        {
            var coffees = (await _mongoDatabase.GetCollection<Coffee>(MongoDbSettings.CoffeeCollection)
                .FindAsync(new BsonDocument()))
                .ToList();

            if (coffees.Any(c => c.IngredientIds.Contains(id)))
                return;

            var ingredients = _mongoDatabase.GetCollection<Ingredient>(MongoDbSettings.IngredientCollection);

            await ingredients.DeleteOneAsync(ingredient => ingredient.Id == id);
        }

        public async Task<IList<Ingredient>> GetAll()
        {
            return (await _mongoDatabase.GetCollection<Ingredient>(MongoDbSettings.IngredientCollection)
                                 .FindAsync(new BsonDocument())).ToList();
        }

        public async Task Update(Ingredient updatedIngredient, string idToUpdate)
        {
            var ingredients = _mongoDatabase.GetCollection<Ingredient>(MongoDbSettings.IngredientCollection);

            await ingredients.ReplaceOneAsync(ingredient => ingredient.Id == idToUpdate, updatedIngredient);
        }
    }
}
