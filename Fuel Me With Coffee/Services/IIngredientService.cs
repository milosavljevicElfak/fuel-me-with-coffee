﻿using Fuel_Me_With_Coffee.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fuel_Me_With_Coffee.Services
{
    public interface IIngredientService
    {
        public Task Create(Ingredient ingredient);

        public Task<IList<Ingredient>> GetAll();

        public Task Update(Ingredient updatedIngredient, string idToUpdate);

        public Task Delete(string id);

    }
}
