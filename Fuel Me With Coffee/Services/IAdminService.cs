using System.Threading.Tasks;
using Fuel_Me_With_Coffee.Models;

namespace Fuel_Me_With_Coffee.Services
{
    public interface IAdminService
    {
        public Task<bool> Login(Admin admin);
    }
}