using System.Threading.Tasks;
using Fuel_Me_With_Coffee.Models;
using MongoDB.Driver;

namespace Fuel_Me_With_Coffee.Services
{
    public class AdminService : IAdminService
    {
        private readonly IMongoClient _mongoClient;
        private readonly IMongoDatabase _mongoDatabase;
        private readonly string defaultAdminUsername = "admin";
        private readonly string defaultAdminPassword = "root";


        public AdminService(IMongoClient mongoClient)
        {
            _mongoClient = mongoClient;
            _mongoDatabase = _mongoClient.GetDatabase(MongoDbSettings.DatabaseName);
        }

        public async Task<bool> Login(Admin admin)
        {
            if (admin == null || admin.Username != defaultAdminUsername || admin.Password != defaultAdminPassword)
                return false;

            var cursor = await _mongoDatabase.GetCollection<Admin>(MongoDbSettings.AdminCollection)
                .FindAsync(a => a.Username == admin.Username && a.Password == admin.Password);

            var adminFromDb = await cursor.FirstOrDefaultAsync();

            if (adminFromDb == null)
            {
                await CreateDefaultAdminAsync(admin);
            }

            return true;
        }

        private async Task CreateDefaultAdminAsync(Admin admin)
        {
            var adminCollection = _mongoDatabase.GetCollection<Admin>(MongoDbSettings.AdminCollection);
            await adminCollection.InsertOneAsync(admin);
        }
    }
}