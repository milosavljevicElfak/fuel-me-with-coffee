﻿using Fuel_Me_With_Coffee.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fuel_Me_With_Coffee.Services
{
    public class CoffeeService : ICoffeeService
    {
        private readonly IMongoClient _mongoClient;
        private readonly IMongoDatabase _mongoDatabase;

        public CoffeeService(IMongoClient mongoClient)
        {
            _mongoClient = mongoClient;
            _mongoDatabase = _mongoClient.GetDatabase(MongoDbSettings.DatabaseName);
        }

        public async Task Create(Coffee coffee)
        {
            var coffees = _mongoDatabase.GetCollection<Coffee>(MongoDbSettings.CoffeeCollection);
            await coffees.InsertOneAsync(coffee);
        }

        public async Task Delete(string id)
        {
            var coffees = _mongoDatabase.GetCollection<Coffee>(MongoDbSettings.CoffeeCollection);
            await coffees.DeleteOneAsync(coffee => coffee.Id == id);
        }

        public async Task<IList<CoffeeViewModel>> GetAll()
        {
            var coffees = (await _mongoDatabase.GetCollection<Coffee>(MongoDbSettings.CoffeeCollection).FindAsync(new BsonDocument()))
                            .ToList();
            var ingredients = (await _mongoDatabase.GetCollection<Ingredient>(MongoDbSettings.IngredientCollection).FindAsync(new BsonDocument()))
                            .ToList();

            var coffeesModals = coffees.Select(coffee =>
                new CoffeeViewModel()
                {
                    Id = coffee.Id,
                    Name = coffee.Name,
                    Ingredients = coffee.IngredientIds.Select(idOfIngredient =>
                        ingredients.Find(ingredient => ingredient.Id == idOfIngredient)
                    ).ToList()
                }
            );

            return coffeesModals.ToList();
        }

        public async Task<CoffeeViewModel> GetById(string id)
        {
            var cursor = await _mongoDatabase
            .GetCollection<Coffee>(MongoDbSettings.CoffeeCollection)
            .FindAsync(c => c.Id == id);

            var coffee = await cursor.FirstOrDefaultAsync();

            var ingredients = (await _mongoDatabase.GetCollection<Ingredient>(MongoDbSettings.IngredientCollection).FindAsync(new BsonDocument()))
                            .ToList();

            var coffeesModal = new CoffeeViewModel()
            {
                Name = coffee.Name,
                Ingredients = coffee.IngredientIds.Select(idOfIngredient =>
                    ingredients.Find(ingredient => ingredient.Id == idOfIngredient)
                ).ToList()
            };

            return coffeesModal;
        }

        public async Task Update(Coffee updatedCoffee, string idToUpdate)
        {
            var coffees = _mongoDatabase.GetCollection<Coffee>(MongoDbSettings.CoffeeCollection);

            await coffees.ReplaceOneAsync(coffee => coffee.Id == idToUpdate, updatedCoffee);
        }
    }
}
