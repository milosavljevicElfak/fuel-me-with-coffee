﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fuel_Me_With_Coffee
{
    public static class MongoDbSettings
    {
        public static string DatabaseName => "fuel-me-with-coffee"; 

        public static string CoffeeCollection => "coffee";
        public static string AdminCollection => "admin";
        public static string IngredientCollection => "ingredient";

    }
}
