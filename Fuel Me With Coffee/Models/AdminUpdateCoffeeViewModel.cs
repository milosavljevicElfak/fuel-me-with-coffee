using System.Collections.Generic;

namespace Fuel_Me_With_Coffee.Models
{
    public class AdminUpdateCoffeeViewModel
    {
        public CoffeeViewModel Coffee { get; set; }
        public IList<Ingredient> AllAvailableIngredients { get; set; }
    }
}