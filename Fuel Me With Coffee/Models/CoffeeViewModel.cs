using System.Collections.Generic;

namespace Fuel_Me_With_Coffee.Models
{
    public class CoffeeViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public IList<Ingredient> Ingredients { get; set; }
    }
}