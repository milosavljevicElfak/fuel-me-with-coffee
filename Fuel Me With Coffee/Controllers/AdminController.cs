﻿using Fuel_Me_With_Coffee.Models;
using Fuel_Me_With_Coffee.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Fuel_Me_With_Coffee.Sessions;

namespace Fuel_Me_With_Coffee.Controllers
{
    public class AdminController : Controller
    {
        private readonly ICoffeeService _coffeeService;
        private readonly IIngredientService _ingredientService;
        private readonly IAdminService _adminService;


        public AdminController(
            ICoffeeService coffeeService,
            IIngredientService ingredientService,
            IAdminService adminService)
        {
            _coffeeService = coffeeService;
            _ingredientService = ingredientService;
            _adminService = adminService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> AddCoffee()
        {
            return View(await _ingredientService.GetAll());
        }

        [HttpPost]
        public async Task<IActionResult> AddCoffee(Coffee coffee)
        {
            if (coffee == null
                || string.IsNullOrEmpty(coffee.Name)
                || coffee.IngredientIds == null
                || coffee.IngredientIds.Count == 0)
                return Json(new { redirectToUrl = Url.Action("AddCoffee") });

            await _coffeeService.Create(coffee);

            return Json(new { redirectToUrl = Url.Action("AddCoffee") });
        }

        public async Task<IActionResult> GetCoffee()
        {
            return View("ChangeCoffee", await _coffeeService.GetAll());
        }

        public async Task<IActionResult> GetIngredient()
        {
            return View("ChangeIngredient", await _ingredientService.GetAll());
        }

        public async Task<IActionResult> RemoveCoffee(string id)
        {
            await _coffeeService.Delete(id);

            return RedirectToAction("GetCoffee");
        }


        [HttpGet]
        public async Task<IActionResult> UpdateCoffee(string id)
        {
            var coffee = await _coffeeService.GetById(id);
            var ingredients = await _ingredientService.GetAll();

            return View(new AdminUpdateCoffeeViewModel()
            {
                Coffee = coffee,
                AllAvailableIngredients = ingredients
            });
        }

        [HttpPut]
        public async Task<IActionResult> UpdateCoffee(Coffee coffee)
        {
            if (coffee == null)
                return NotFound();

            await _coffeeService.Update(coffee, coffee.Id);

            return Json(new { redirectToUrl = Url.Action("GetCoffee") });
        }

        [HttpGet]
        public IActionResult AddIngredient()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddIngredient(Ingredient ingredient)
        {
            if (string.IsNullOrEmpty(ingredient.Name))
                return View();

            await _ingredientService.Create(ingredient);

            return RedirectToAction("AddIngredient");
        }

        public async Task<IActionResult> RemoveIngredient(string id)
        {
            await _ingredientService.Delete(id);

            return RedirectToAction("GetIngredient");
        }


        [HttpPost]
        public async Task<IActionResult> UpdateIngredient(Ingredient ingredient)
        {
            if (ingredient != null)
                await _ingredientService.Update(ingredient, ingredient.Id);

            return RedirectToAction("GetIngredient");
        }

        [HttpGet]
        public IActionResult Login()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetAdminUsername()))
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(Admin admin)
        {
            if (await _adminService.Login(admin))
                HttpContext.Session.StoreAdminUsername(admin.Username);

            return RedirectToAction("Login");
        }
    }
}
