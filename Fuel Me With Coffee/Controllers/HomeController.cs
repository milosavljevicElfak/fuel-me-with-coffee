﻿using Fuel_Me_With_Coffee.Services;
using Microsoft.AspNetCore.Mvc;
using Fuel_Me_With_Coffee.Sessions;
using System.Threading.Tasks;

namespace Fuel_Me_With_Coffee.Controllers
{
    public class HomeController : Controller
    {
        private readonly IIngredientService _ingredientService;
        private readonly ICoffeeService _coffeeService;

        public HomeController(ICoffeeService coffeeService, IIngredientService ingredientService)
        {
            _coffeeService = coffeeService;
            _ingredientService = ingredientService;
        }

        public async Task<IActionResult> Index()
        {
            HttpContext.Session.RemoveAdminUsername();
            return View(await _coffeeService.GetAll());
        }

        public async Task<IActionResult> MakeCoffee()
        {
            HttpContext.Session.RemoveAdminUsername();
            return View(await _ingredientService.GetAll());
        }

    }
}
