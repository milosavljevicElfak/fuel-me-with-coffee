﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fuel_Me_With_Coffee.Sessions
{
    public static class FuelMeWithCoffeeSession
    {
        private static readonly string adminUsernameKey = "session.admin.username";

        public static void StoreAdminUsername(this ISession session, string username)
        {
            session.SetString(adminUsernameKey, username);
        }

        public static string GetAdminUsername(this ISession session)
        {
            return session.GetString(adminUsernameKey);
        }

        public static void RemoveAdminUsername(this ISession session)
        {
            session.Remove(adminUsernameKey);
        }
    }
}
